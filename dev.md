# Fuseki #

## CAN NOT DO ##

- multiple identical values for one subject and predicate
  - identical for nodes means id, for literals means value+type+language
  

## Goals ##

### For Entities

- ✔ can be used in vue store
- ✔ can calculate changes and persist them

### pseudo-anon objects (child-nodes)

- ✔ linked to parents by id
  - optionally other ref
- ✔ persisted, moved with them
- ✔ loaded with them


### For graph

- ✔ move nodes between graphs
  - ✔ must set node.graph
  - ✔ clone node
  - ✔ what to do with linked nodes?
    - ✔ depth parameter for cloning
    
- ✔ be accessible for vue reactivity
- ✔ be compatible with json-ld
  - as from-data / toData or ✔ directly

- iterate over all in batches
- migrations
- list all graphs, with tags

### For Named Graphs

- ✔ can persist into one specific named graph
- ✔ can load from specified graphs

#### usage options

- one graph per entity
  - make it easy to find and load related anon objects
  - graph id is entity main id
- one graph per layer
  - easy to load basics all in one

## How To...

### have storage in multiple named graphs

- have regular one graph storage first to save and load
- have a read-only meta-storage that takes in the regular ones as arguments


### have inheritance classes

- define properties that are looked up in parent class
- have a proxy to trap get calls, or use .get
- can set other props than type to check, even multiple

### support for lists ###


## loadmaps in graphs

- consider if moving objects between graphs?
  - no
- consider when creating the graph?
  - yes, simple
- can be changed later?
  - no
- add new node with id?
  - yes
- implement how?
  - map @type to constructor
  - give fn that returns constructed
    - opt fn that returns child-node
it's only really possible based on @type.
but that means child-nodes are out.

## upload

- option for documentstorage
  - transform a json
- option for keyvaluestorage