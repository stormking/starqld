# StarqLD #

A lib that uses json-ld and sparql to make working with RDF/SPARQL servers easy.

## Feature Summary ##

- abstraction for easy key-value storage
- abstraction for document storage
- abstraction for full graph usage (in JSON-LD form)
- query builder
- abstraction for pseudo-anonymous nodes
- class/object based handling of data
- TypeScript support
- supports using the models directly in reactive frameworks like vue.js
- CLI actions (assert, upload, validate)
- runs in node.js and in the browser

## Status ##

- alpha quality, has only rudimentary tests
- under heavy development
- not yet published to npm

## Roadmap ##

- apply migration to rdf db
- list graphs, all or by tags
- subclasses for nodes and docs
- proper validation options
- support SPARQL endpoints other than Fuseki

## Example ##

### Graph Storage

```js
import { FusekiRepo, Graph, Literal } from '../../src/index';
//configure the endpoint
const repo = new FusekiRepo('http://admin:admin@localhost:3030/test1')
//create test1 if it does not exist
await repo.assertCollection();
const id1 = 'http://example.org/test/entity1';
const prop1 = 'http://example.org/test/prop/label';
// see https://json-ld.org/ for the data syntax
let graph = await Graph.createNormalized({
    '@id': id1,
    '@type': 'http://example.org/test/type1',
    [prop1]: 'hello world'
});
//handling the nodes
let node = graph.getNodeById(id1);
node[prop1] = 'hello change'
console.log(node.get(prop1))    // same as node[prop1] - Literal{ @value: 'hello change' }
console.log(node.getLiteral(prop1)) //hello change (parses value according to type)
console.log(node.getAll(prop1)) // Collection[ Literal{ @value: 'hello change' } ] (always returns as collection)

node.addToProp(prop1, new Literal({ '@value': 'hole mundo', '@language': 'es' }))
//this adds to the prop instead of overwriting it
console.log(node.get(prop1))    //Collection[ Literal{ @value: 'hello change' }, Literal{ @value: 'hola mundo', @language: 'es' } ]

//init a named graph for storage
const storage = repo.assertGraphStorage('store1')
await storage.persistGraph(graph, { create: true }) 
// the create option will persist all data, not just that which was changed after init


//load the data
const qb = storage.createDescribeQuery();
qb.addIdToFilter(id1);
graph = await storage.load(qb);

```

For more examples check out the tests in `test/integration/*`

### Document Storage

...

### Key-Value Storage

...

### CLI Actions

...

## CAN NOT DO ##

These things are outside the capabilities of the library and this will never change:

- multiple identical values for one subject and predicate
  - identical for nodes means id, for literals means value+type+language
