import Repo from './service/fuseki.js';
// import Validator from './service/validator.js';
import parser from 'yargs-parser';
import FS from 'fs';

const argv = parser(process.argv);
console.log(argv);
const repoUri = argv.r || argv.repo;
if (!repoUri) throw new Error('missing --repo/-r');
console.log('using '+repoUri);
const repo = new Repo(repoUri);

async function run() {
	if (argv.destroy) {
		console.log('destroying collection...')
		await repo.destroyCollection();
	}
	if (argv.assert) {
		console.log('asserting collection exists...')
		await repo.assertCollection();
	}
	if (argv.upload) {
		let files = Array.isArray(argv.upload) ? argv.upload : [argv.upload];
		for (let file of files) {
			console.log('uploading '+file+' ...');
			let content = await FS.promises.readFile(file, { encoding: 'utf8' });
			if (file.match(/\.ttl$/)) {
				await repo.uploadTurtle(content);
			} else {
				await repo.upload(content);
			}
		}
	}
	// if (argv.validate) {
	// 	let validator = new Validator(repo);
	// 	console.log('running validation...');
	// 	await validator.run();
	// 	let errors = validator.getErrors();
	// 	if (!errors.size) {
	// 		console.log('no errors');
	// 	} else {
	// 		console.log(errors);
	// 	}
	// }
}

run()
