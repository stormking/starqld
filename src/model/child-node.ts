import Graph from "./graph.js";
import Node from "./node.js";

const parentSymbol = Symbol('parent')

export default class ChildNode extends Node {

    static TYPE = 'http://starqld.cryptic.link/owl/type/childnode';
    static ID_PREFIX = 'http://starqld.cryptic.link/owl/childnode#';
    static PARENT_PROP = 'http://starqld.cryptic.link/owl/prop/parent';
    [parentSymbol]: Node

    constructor(obj: any, root: Graph, parent: Node) {
        super(obj, root)
        this[parentSymbol] = parent;
    }

    getParent(): Node {
        let n = this.get(ChildNode.PARENT_PROP)
        if (n instanceof Node) {
            return n;
        }
        throw new Error('invalid prop set for '+ChildNode.PARENT_PROP);
    }

}
