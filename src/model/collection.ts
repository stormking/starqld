import Graph from "./graph.js";
import Literal from "./literal.js";
import Node from "./node.js";


export default class Collection extends Array {
	
	get one() {
		return this[0];
	}

	map(fn): Collection {
		let a: any = super.map(fn)
		return a;
	}

	concat(...items: unknown[]): Collection {
		let a: any = super.concat(items);
		return a;
	}

	// filter(predicate: (value: any, index: number, array: Collection) => value is any, thisArg?: unknown): Collection {
	// 	let a: any = super.filter(predicate, thisArg);
	// 	return a;
	// }

	remove(e: any) {
		let idx = this.findIndex(o => o === e);
		if (idx > -1) this.splice(idx, 1);
	}

	clone(toGraph: Graph, depth: number): Collection {
		return this.map(function(value, i) {
			if (value instanceof Node) {
				return value.clone(toGraph, depth)
			} else if (value instanceof Literal) {
				return value.clone();
			}
			return value;
		})
	}

}
