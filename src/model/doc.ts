import { diffLines } from 'diff';
import flatModule from 'flat';
const { flatten, unflatten } = flatModule
import { v4 as uuid } from 'uuid'

const _origData = Symbol('origData');

export default class Doc {

    static META_PREFIX = 'http://starqld.org/owl/doc/';
	static PATH_PREFIX = this.META_PREFIX+'path#';

    [_origData]: Record<string, any>

    #data: Record<string, any>
    #created: Date
    #updated: Date
    #rev: number
    #id: string

    constructor() {

    }

    public setData(d: Record<string, any>) {
        this.#data = d;
    }

    public getData(): Record<string, any> {
        return this.#data;
    }

    public getCreated(): Date {
        return this.#created
    }

    public getUpdated(): Date {
        return this.#updated
    }

    public getRev(): number {
        return this.#rev
    }

    public getId(): string {
		if (!this.#id) return '';
        return this.#id;
    }

    public static fromStorage(res: any): Doc {
        const doc = new Doc();
        let flat = {};
        const pathPrefix = this.PATH_PREFIX;
        const pathStart = pathPrefix.length;
		let getDate = function(val) {
			let v = Array.isArray(val) ? val[0] : val;
			v = typeof v === 'object' && '@value' in v ? v['@value'] : v;
			return new Date(v)
		};
        Object.keys(res).forEach(key => {
            const val = res[key];
            if (key === '@id') {
                doc.#id = val;
            } else if (key.substring(0, pathStart) === pathPrefix) {
                let v;
                if (typeof val === 'string') v = val;
                else if (val['@type'] === 'xsd:boolean') {
                    v = val['@value'] === 'true'
                } else if (val['@type'] === 'xsd:integer') {
                    v = parseInt(val['@value'])
                } else if (val['@type'] === 'xsd:float') {
                    v = parseFloat(val['@value'])
                } else if (val['@type'] === 'xsd:dateTime') {
                    v = getDate(val)
                } else if (val['@type'] === 'json:null') {
                    v = null
                } else if (['json:array', 'json:object'].includes(val['@type'])) {
                    v = JSON.parse(val['@value'])
                } else {
                    v = val['@value']
                }
                flat[key.substring(pathStart)] = v;
            } else if (key === this.META_PREFIX+'rev') {
                doc.#rev = ~~val['@value']
            } else if (key === this.META_PREFIX+'created') {
                doc.#created = getDate(val);
            } else if (key === this.META_PREFIX+'updated') {
                doc.#updated = getDate(val);
            }
        })
        // console.dir(res, { depth: 9 })
        doc.setData(unflatten(flat));
        doc.lockInitialData();
        return doc;
    }

    protected static getRdf(id: string, flatData: Record<string, any>, keysAreIds: boolean): string[] {
		// console.debug('getRdf', flatData);
        return Object.entries(flatData).map(([k, v]) => {
			let type = Array.isArray(v) ? 'array' : typeof v;
			if (type === 'undefined') return '';
			let val: string;
			let json = JSON.stringify(v);
			let wrapped = json[0] === '"';
			let wrap = wrapped ? '' : '"';
			let fullType: string;
			if (type === 'string') {
				fullType = 'xsd:string'
			} else if (type === 'number') {
				if (v % 1 > 0) fullType = 'xsd:float';
				else fullType = 'xsd:integer';
			} else if (type === 'boolean') {
				fullType = 'xsd:boolean'
			} else if (type === 'array') {
				fullType = 'json:array'
			} else if (v === null) {
				fullType = 'json:null'
			} else if (v instanceof Date) {
				fullType = 'xsd:dateTime'
			} else {
				fullType = 'json:'+type;
			}
			val = `${wrap}${JSON.stringify(v)}${wrap}^^${fullType}`
			return `<${id}> ${keysAreIds ? '<'+k+'>' : '<'+this.PATH_PREFIX+k+'>'} ${val} .`
		})
    }

    getMeta() {
        return {
            [Doc.META_PREFIX+'rev']: this.#rev,
            [Doc.META_PREFIX+'created']: this.#created,
            [Doc.META_PREFIX+'updated']: this.#updated
        }
    }

    getChanges() {
		// console.log('GET CHANGES', this[Node._origData]);
        const meta = this.getMeta();
        const newMeta: any = Object.fromEntries(Object.entries(meta));
        newMeta[Doc.META_PREFIX+'rev'] += 1;
        newMeta[Doc.META_PREFIX+'updated'] = new Date();
		const oldRdf = this[_origData] ? Doc.getRdf(this.#id, this[_origData], false)
            .concat(Doc.getRdf(this.#id, flatten(meta), true))
            .join('\n') : '';
		const newRdf = Doc.getRdf(this.#id, flatten(this.getData()), false)
            .concat(Doc.getRdf(this.#id, flatten(newMeta), true))
            .join('\n');
		return this.getChangeFromRdf(oldRdf, newRdf);
	}

	getChangesToDelete() {
		// console.log('getChangesToDelete', this)
		const meta = this.getMeta();
		const oldRdf = this[_origData] ? Doc.getRdf(this.#id, this[_origData], false)
            .concat(Doc.getRdf(this.#id, flatten(meta), true))
            .join('\n') : '';
		const newRdf = '';
		const changes = this.getChangeFromRdf(oldRdf, newRdf);
		return changes.removed;
	}

	protected getChangeFromRdf(oldRdf: string, newRdf: string) {
		const res = diffLines(oldRdf, newRdf)
		// console.dir(res, { depth: 9 })
		const added: string[] = [];
		const removed: string[] = [];
		res.flatMap(e => {
			return e.value.split('\n').map(line => ({
				value: line,
				removed: e.removed,
				added: e.added
			}))
		}).forEach(e => {
			let v = e.value.trim();
			if (!v) return;
			if (e.removed) removed.push(v)
			else if (e.added) added.push(v)
		});
		return { added, removed }
	}

    lockInitialData() {
        let data = this.getData();
		Object.defineProperty(this, _origData, {
			value: flatten(data),
			enumerable: false,
			configurable: true,
			writable: true
		})
	}

    increaseRev() {
        this.#rev += 1;
        this.#updated = new Date();
    }

    initNew() {
        this.#id = Doc.META_PREFIX+'id#'+uuid()
        this.#rev = 0;
        this.#created = new Date();
        this.#updated = new Date();
    }

	toJSON() {
		return {
			id: this.getId(),
			rev: this.getRev(),
			created: this.getCreated(),
			updated: this.getUpdated(),
			data: this.getData()
		}
	}

}
