import type { JsonLdDocument } from 'jsonld'
import Collection from "./collection.js";
import Node from "./node.js";
import jsonld from 'jsonld'
import Literal from './literal.js';
import ChildNode from './child-node.js';
import { v4 as uuid } from 'uuid'
import { traverse } from '../util/util.js';

type AddOptions = {
	depth?: number
}

export type LoadMap = {
	[key: string]: Node['constructor']
}
export type LoadMapFn = (data, def) => Node["constructor"]

function defaultLoadMap(loadMap?: object): LoadMapFn {
	const map = loadMap || {};
	return function(data, constr): Node["constructor"] {
		let type = data['@type']
		if (type in map) return map[type];
		return constr;
	}
}

export default class Graph {

    private '@graph': Collection
	protected loadMap: LoadMapFn;

    constructor(o?: any) {
        if (o && o['@graph']) {
            this['@graph'] = o['@graph']
        } else if (Array.isArray(o)) {
            let c: any = new Collection().concat(o)
            this['@graph'] = c;
        } else {
            this['@graph'] = new Collection();
        }
		this.loadMap = defaultLoadMap({});
    }

    getGraph(): Collection {
        return this['@graph']
    }

	setGraph(g) {
		// console.log('set graph', g);
		if ('@graph' in g) {
			this['@graph'] = g['@graph'];
		} else if (g instanceof Collection) {
			this['@graph'] = g;
		} else {
			throw new Error('invalid value for graph')
		}
	}

    addExternalNode(other: Node, opts?: AddOptions): Node {
		if (!opts) opts = { depth: 1 }
		let depth = !isNaN(opts.depth) && opts.depth >= 0 ? opts.depth : 1;
        return other.clone(this, depth);
    }

	merge(other: Graph, replaceData?: boolean) {
		other.getGraph().forEach((n: Node) => {
			n.clone(this, 0, replaceData);	//this could probably be 0 but just to be sure
		});
	}

	getNodeById(id: string, safe?: boolean): Node {
		let n = this.getGraph().find(e => e instanceof Node && e.getId() === id);
		if (!n) {
			if (safe) return null;
			else throw new Error('node not found: '+id);
		}
		return n;
	}

    addData(data: any) {
		const root = this;
		const nodeCache = new Map<string, Node>();
		const constrCache = new Map<string, any>()
		const idCache = new Map<string, object>();
		const getById = function(id) {
			if (!nodeCache.has(id)) {
				let node = new Node({ '@id': id }, root);
				nodeCache.set(id, node);
			}
			return nodeCache.get(id)
		}
		traverse(data, function(val, key) {
			if (val && val['@id']) {
				let id = val['@id'];
				if (!idCache.has(id)) {
					idCache.set(id, val)
				} else {
					if (Object.keys(val).length > 1) {
						Object.assign(idCache.get(id), val)
					}
				}
			}
		});
		const setConstr = (data, id) => {
			let isChildNode = id.substring(0, ChildNode.ID_PREFIX.length) === ChildNode.ID_PREFIX;
			let constr;
			let params;
			let node: Node;
			if (isChildNode) {
				let parentId = data[ChildNode.PARENT_PROP]
				let parent = getById(parentId);
				params = [root, parent];
				constr = ChildNode;
			} else {
				params = [root];
				constr = Node;
			}
			constr = this.getNodeConstructor(data, constr);
			constrCache.set(id, [constr, params]);
		}
		for (let [id, data] of idCache.entries()) {
			setConstr(data, id);
		}
		JSON.parse(JSON.stringify(data), (key, val) => {
			// console.log('parse', key, val);
			const t = typeof val;
			if (['number', 'string', 'boolean'].includes(t)) {
				if (key[0] === '@') {
					return val;
				} else {
					return new Literal({ '@value': val });
				}
			}
			if (t === 'object') {
				if ('@value' in val) {
					return new Literal(val);
				}
				if ('@id' in val) {
					let id = val['@id'];
					let node: Node;
					if (!nodeCache.has(id)) {
						let [Constr, params] = constrCache.get(id);
						params.unshift(val);
						node = new Constr(...params)
						nodeCache.set(id, node);
					} else {
						node = nodeCache.get(id);
						node.addData(val);
					}
					return node;
				}
				if (Array.isArray(val)) {
					return new Collection(...val);
				}
			}
			if (key === '') {
				root.setGraph(val);
				root.getGraph().forEach(node => node.lockInitialData())
				return root;
			}
			return val;
		})
    }

	addNewNodeWithId(id: string, type?: string): Node {
		try {
			return this.getNodeById(id)
		} catch (e) {
			const d = { '@id': id };
			if (type) d['@type'] = type;
			let constr = this.getNodeConstructor(d, Node);
			let n = new constr(d, this);
			this.getGraph().push(n);
			return n;
		}
	}

	getNodeConstructor(data, def): any {
		return this.loadMap(data, def);
	}

	addChildNode(propName: string, parentNode: Node): ChildNode {
		const data = {
			'@id': `${ChildNode.ID_PREFIX}${uuid()}`, 
			'@type': ChildNode.TYPE,
			[ChildNode.PARENT_PROP]: parentNode
		};
		const newNode = new ChildNode(data, this, parentNode);
		this.getGraph().push(newNode);
		parentNode.addToProp(propName, newNode);
		return newNode;
	}

    remove(n: Node) {
		this.getGraph().remove(n);
		//@TODO find an purge references in other nodes
	}

	async exportJSON(frame?: any) {
		return await jsonld.frame(JSON.parse(JSON.stringify(this)), frame)
	}

	setLoadMap(map: LoadMapFn) {
		this.loadMap = map;
	}

	static createEmpty(loadMap?: LoadMap|LoadMapFn): Graph {
		const root = new Graph();
		if (typeof loadMap === 'function') {
			root.setLoadMap(loadMap);
		} else {
			root.setLoadMap(defaultLoadMap(loadMap))
		}
		return root;
	}

	static async createNormalized(json: JsonLdDocument, loadMap?: LoadMap|LoadMapFn): Promise<Graph> {
		let flat: any = await jsonld.flatten(json, {})
		// console.dir({ flat }, { depth: 9 })
		const root = new Graph();
		if (typeof loadMap === 'function') {
			root.setLoadMap(loadMap);
		} else {
			root.setLoadMap(defaultLoadMap(loadMap))
		}
		root.addData(flat);
		return root;
	}

	toJSON() {
		return {
			'@graph': this.getGraph().map((e: Node) => e.toJSON())
		}
	}

}
