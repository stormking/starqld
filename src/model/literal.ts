
const KEYS = [
	'@value',
	'@type',
	'@language'
]

export default class Literal {

	// private '@value': string|number|boolean
	// private '@type': string
	// private '@language': string

	constructor(obj) {
		KEYS.forEach(key => {
			if (key in obj && (obj[key] || key === KEYS[0])) {
				this[key] = obj[key];
			}
		})
	}

	getType() {
		return this['@type'];
	}

	getLanguage() {
		return this['@language'];
	}

	getRawValue() {
		return this['@value'];
	}

	getValue() {
		const t = this.getType();
		const v = this['@value'];
		if (typeof v !== 'string' || !t) return v;
		if (t.includes('float')) {
			return parseFloat(v)
		}
		if (t.includes('integer') || t.includes('number')) {
			return parseInt(v);
		}
		if (t.includes('boolean')) {
			return v === 'true';
		}
		// console.warn('unknown type: '+t);
		return v;
	}

	toJSON() {
		let r = [['@value',this['@value']]]
		let t = this.getType();
		let l = this.getLanguage();
		if (t) r.push(['@type', t])
		if (l) r.push(['@language', l])
		return Object.fromEntries(r);
	}

	toString() {
		return `${this.getValue()}`
	}

	clone() {
		return new Literal(this.toJSON())
	}

	getCompareValue() {
		return `${this.getRawValue()}_${this.getType()}_${this.getLanguage()}`
	}
}
