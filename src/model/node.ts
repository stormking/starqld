import Graph from "./graph.js";
import jsonld from 'jsonld'
import { diffLines } from 'diff';
import Collection from "./collection.js";
import Literal from "./literal.js";
import type ChildNode from "./child-node.js";

const graph = Symbol('graph');
const rdfOpts: jsonld.Options.ToRdf = {
	format: 'application/n-quads'
}
function canBeCollection(propName: string) {
	return propName[0] !== '@'
}

type ToDataOpts = {
	inclSubnodes?: boolean
}
type ReplaceOpts = {
	includeOrigData?: boolean
}

const graphSymbol = Symbol('graph')
const origSymbol = Symbol('origData')
const ChildNodeType = 'http://starqld.cryptic.link/owl/type/childnode';

export default class Node {

	[graphSymbol]: Graph
	[origSymbol]: Record<string, any>

	constructor(obj, root: Graph) {
		this[graphSymbol] = root;
		this.addData(obj);
	}

	addChildNode(propName: string): ChildNode {
		return this[graphSymbol].addChildNode(propName, this);
	}

	addToProp(propName: string, value) {
		if (!canBeCollection(propName)) {
			this[propName] = value;
			return;
		}
		let existing = this[propName];
		if (!existing) {
			this[propName] = value;
			return;
		}
		let wasCollection = Array.isArray(value) || Array.isArray(existing);
		if (!Array.isArray(value)) {
			value = new Collection().concat(value)
		}
		if (!Array.isArray(existing)) {
			existing = new Collection().concat(existing)
		}
		value.forEach(val => {
			let valExists = existing.find(e => {
				if(!val.getCompareValue || !e.getCompareValue) {
					return false;
				}
				return e.getCompareValue() === val.getCompareValue()
			});
			if (!valExists) {
				existing.push(val)
			}
		})
		if (!wasCollection && existing.length === 1) {
			// console.log('reducing to one again', existing, value)
			this[propName] = existing.one;
		} else {
			this[propName] = existing;
		}
	}

	addData(obj) {
		Object.entries(obj).forEach(([k, v]) => {
			this.addToProp(k, v);
			// this[k] = v;
		})
	}

	replaceData(obj: Node|Record<string,any>, replaceOptions?: ReplaceOpts) {
		if (!obj) obj = {};
		const keys = Object.keys(obj);
		const removeKeys = Object.keys(this).filter(k => k[0] !== '@' && !keys.includes(k))
		keys.forEach(key => {
			let val = obj[key];
			if (val && val['@id']) {
				val = this.getGraph().addNewNodeWithId(val['@id'])
			}
			this[key] = val
		})
		removeKeys.forEach(key => {
			delete this[key]
		})
		if (replaceOptions?.includeOrigData) {
			if (obj instanceof Node) {
				this[origSymbol] = obj[origSymbol] ? JSON.parse(JSON.stringify(obj[origSymbol])) : {}
			} else {
				console.error('replaceData Source: ', obj);
				throw new Error('cannot read origData, source is not a Node');
			}
		}
	}

	lockInitialData() {
		this[origSymbol] = this.toData();
	}

	destroy() {
		this[graphSymbol].remove(this);
		Object.keys(this).forEach(k => {
			delete this[k]
		})
	}

	setId(id: string) {
		this['@id'] = id;
	}

	getId(): string {
		return this['@id'] || ''
	}

	getType(): string {
		return this['@type'] || ''
	}

	setType(t: string) {
		this['@type'] = t;
	}

	toData(opts?: ToDataOpts) {
		let data = Node.toData(this, opts);
		// console.log('ToData', opts, data);
		return data;
	}


	static toData(node: Node, opts?: ToDataOpts) {
		let seenNodes = new Set<Node>();
		seenNodes.add(node);
		if (!opts) opts = {};
		const maxDepth = !!opts.inclSubnodes ? 2 : 0;
		function handleValue(o, depth: number) {
			if (o instanceof Node) {
				if (!o.getId()) return;
				let seen = seenNodes.has(o);
				seenNodes.add(o);
				if (depth < 1 || seen) {
					o = { '@id': o.getId() };
				} else {
					o = handleNode(o, depth);
				}
			}
			if (o instanceof Literal) {
				return o.toJSON();
			}
			return o;
		}
		function handleCollection(val: Collection|any[], depth: number) {
			let arr = [];
			val.forEach(o => {
				arr.push(handleValue(o, depth));
			})
			return arr;
		}
		function handleNode(node: Node, depth: number) {
			let res = [];
			for (let propName of Object.keys(node)) {
				let val = node[propName];
				if (val instanceof Collection || Array.isArray(val)) {
					val = handleCollection(val, depth-1);
				} else {
					val = handleValue(val, depth-1)
				}
				res.push([propName, val]);
			}
			return Object.fromEntries(res);
		}
		return handleNode(node, maxDepth);
	}

	toJSON() {
		return Object.fromEntries(Object.entries(this).map(([k, v]) => {
			if (v instanceof Collection) {
				let c = v;
				v = [];
				c.forEach(o => {
					if (o instanceof Node) {
						o = { '@id': o.getId() };
					}
					v.push(o);
				})
			} else if (v instanceof Node) {
				v = { '@id': v.getId() };
			}
			return [k, v];
		}));
	}

	async getChanges(all?: boolean, skipChildNodes?: boolean) {
		const oldRdf: any = all ? '' : await jsonld.toRDF(this[origSymbol], rdfOpts);
		const newRdf: any = await jsonld.toRDF(this.toData({ inclSubnodes: !skipChildNodes }), rdfOpts);
		let res = diffLines(oldRdf, newRdf)
		// console.dir({ res, oldRdf, newRdf }, { depth: 9 })
		const added: string[] = [];
		const removed: string[] = [];
		res.flatMap(e => {
			return e.value.split('\n').map(line => ({
				value: line,
				removed: e.removed,
				added: e.added
			}))
		}).forEach(e => {
			let v = e.value.trim();
			if (!v) return;
			if (e.removed) removed.push(v)
			else if (e.added) added.push(v)
		});
		return { added, removed }
	}


	get(propName: string): Collection|Node|Literal|null {
		let val;
		if (propName in this) {
			val = this[propName];
		} else {
			// if (this.#graph.isInheritanceEnabled()) {
			// 	let parent = this.#graph.getNodeById(this.getType(), true);
			// 	if (parent) {
			// 		return parent.get(propName)
			// 	}
			// }
		}
		if (!val) return null;
		return val;
	}

	getOne(prop: string): Node|Literal|null {
		let base = this.get(prop);
		if (base instanceof Collection) {
			return base.one;
		}
		return base;
	}

	getOneNode(prop: string): Node {
		let r = this.getOne(prop);
		if (!(r instanceof Node)) throw new Error('prop is not a Node: '+prop);
		return r;
	}

	getOneLiteral(prop: string): Literal {
		let r = this.getOne(prop);
		if (!(r instanceof Literal)) throw new Error('prop is not a Literal: '+prop);
		return r;
	}

	getAll(prop: string): Collection {
		let base = this.get(prop);
		if (base instanceof Collection) {
			return base;
		} else {
			let c = new Collection();
			if (base !== null) c.push(base);
			return c;
		}
	}

	getLiteral(prop: string) {
		let base = this.getOne(prop);
		if (base instanceof Literal) {
			return base.getValue();
		}
		return base;
	}

	setLiteral(prop: string, value: any, type?: string, lang?: string) {
		let o = { '@value': value };
		if (type) o['@type'] = type;
		if (lang) o['@language'] = lang;
		this[prop] = new Literal(o);
	}

	setLink(prop: string, id: string): Node {
		let node = this.getGraph().addNewNodeWithId(id);
		this[prop] = node;
		return node;
	}

	addLink(prop: string, id: string): Node {
		let node = this.getGraph().addNewNodeWithId(id);
		this.addToProp(prop, node);
		return node;
	}

	static createEmpty(): Node {
		return new Node({}, new Graph());
	}

	clone(toGraph: Graph, depth: number, replaceData?: boolean): Node {
		let newNode = toGraph.addNewNodeWithId(this.getId(), this.getType());
		if (depth > -1) {
			let entries = [];
			Object.keys(this).forEach((propName) => {
				let value = this.get(propName);
				if (typeof value === 'undefined' || value === null) return;
				if (value.clone) value = value.clone(toGraph, depth-1);
				if (value) entries.push([propName, value]);
			})
			let data = Object.fromEntries(entries);
			if (replaceData) newNode.replaceData(data)
			else newNode.addData(data)
		}
		return newNode;
	}

	*iterateOverAllNodes(maxDepth?: number, _seen = new WeakSet()): IterableIterator<Node> {
		if (isNaN(maxDepth)) maxDepth = 1;
		_seen.add(this);
		yield this;
		if (maxDepth < 1) return;
		for (let val of Object.values(this)) {
			if (val instanceof Collection) {
				for (let n of val) {
					if (n instanceof Node) {
						for (let s of n.iterateOverAllNodes(maxDepth-1, _seen)) {
							yield s;
						}
					}
				}
			}
		}
	}

	getCompareValue() {
		return this.getId()
	}

	getGraph(): Graph {
		return this[graphSymbol]
	}

	getIncomingLinks() {
		let res: [Node, string][] = [];
		this.getGraph().getGraph().forEach(node => {
			for (let key of Object.keys(node)) {
				let obj = node[key];
				if (Array.isArray(obj)) {
					if (obj.find(e => e === this)) {
						res.push([node, key]);
						break;
					}
				} else if (obj instanceof Node) {
					if (obj === this) {
						res.push([node, key]);
						break;
					}
				}
			}
		});
		return res;
	}
}
