import Doc from '../model/doc.js'
import Node from '../model/node.js'
import SelectBuilder from './qb/select.js'
import AskBuilder from './qb/ask.js'
import type { BaseStorageOptions } from './storage/base.js'
import KeyValueStorage from './storage/keyvalue.js'
import DocumentStorage from './storage/document.js'
import GraphStorage from './storage/graph.js'
import MultiGraphStorage from './storage/multigraph.js'
import DescribeBuilder from './qb/describe.js'
import * as QueryBuilder from './qb/main.js'
import BaseStorage from './storage/base.js'
import Graph from '../model/graph.js'
import { Literal } from '../index.js'

type ExposedPromise<T> = typeof Promise & { resolve: Function, reject: Function }

type StorageInitOptions = {
	id: string,
	graphName?: string,
	tags?: string[]
}
type ListStorageFilter = {
	tags?: string[]
}

export default abstract class BaseRepo {

	static META_STORAGE_NAME = 'metastorage';
	static GRAPH_TYPE_PREFIX = 'http://sparql.cryptic.link/owl/graph/type/';
	static GRAPH_TYPE_PROP = 'http://sparql.cryptic.link/owl/graph/type';
	static GRAPH_NAME_PROP = 'http://sparql.cryptic.link/owl/graph/name';
	static GRAPH_TAG_PROP = 'http://sparql.cryptic.link/owl/graph/tag';

	abstract update(query: string): Promise<unknown>;
	abstract query(query: string, acceptMime?: string): Promise<unknown>

	protected metaRepoStorage: GraphStorage
	protected metaRepoInit: Promise<Graph>
	protected loadedStorageMap = new Map<string, BaseStorage>()

	createDescribeQuery(): DescribeBuilder {
		return QueryBuilder.create(QueryBuilder.DESCRIBE);
	}

	createSelectQuery(): SelectBuilder {
		return QueryBuilder.create(QueryBuilder.SELECT);
	}

	createAskQuery(): AskBuilder {
		return QueryBuilder.create(QueryBuilder.ASK);
	}

	getInitializedStorage(id: string): BaseStorage {
		if (!this.loadedStorageMap.has(id)) {
			throw new Error('storage not loaded: '+id);
		}
		return this.loadedStorageMap.get(id);
	}

	getInitializedKeyValueStorage(id: string): KeyValueStorage {
		let store = this.getInitializedStorage(id);
		if (!(store instanceof KeyValueStorage)) {
			throw new Error('storage is of type '+store.constructor.name)
		}
		return store;
	}

	getInitializedDocumentStorage(id: string): DocumentStorage {
		let store = this.getInitializedStorage(id);
		if (!(store instanceof DocumentStorage)) {
			throw new Error('storage is of type '+store.constructor.name)
		}
		return store;
	}

	getInitializedGraphStorage(id: string): GraphStorage {
		let store = this.getInitializedStorage(id);
		if (!(store instanceof GraphStorage)) {
			throw new Error('storage is of type '+store.constructor.name)
		}
		return store;
	}
	
	assertKeyValueStorage(opts: string|StorageInitOptions): Promise<KeyValueStorage> {
		if (typeof opts === 'string') opts = { id: opts };
		return this.initStorage('kv', opts)
	}

	assertDocumentStorage(opts: string|StorageInitOptions): Promise<DocumentStorage> {
		if (typeof opts === 'string') opts = { id: opts };
		return this.initStorage('doc', opts)
	}

	assertGraphStorage(opts: string|StorageInitOptions): Promise<GraphStorage> {
		if (typeof opts === 'string') opts = { id: opts };
		return this.initStorage('graph', opts)
	}

	async assertMultiGraphStorage(ids: string[]): Promise<MultiGraphStorage> {
		const res = await Promise.all(ids.map(id => this.initStorage('graph', { id })))
		const graphNames = res.map((st: BaseStorage) => st.getGraphName())
		return new MultiGraphStorage(this, graphNames);
	}

	async initStorage(type: "kv" | "doc" | "graph", opts: StorageInitOptions): Promise<any> {
		let metaGraph = await this.getMetaRepo();
		if (opts.id.startsWith(BaseStorage.GRAPH_PREFIX)) {
			opts.id = opts.id.substring(BaseStorage.GRAPH_PREFIX.length);
		}
		const graphName = opts.graphName || BaseStorage.GRAPH_PREFIX + opts.id;
		const typeId = BaseRepo.GRAPH_TYPE_PREFIX + type;
		const id = BaseStorage.GRAPH_PREFIX + opts.id;
		if (!graphName) throw new Error('no graphName defined');
		let metaNode = metaGraph.addNewNodeWithId(id);

		let storedType = metaNode.getOne(BaseRepo.GRAPH_TYPE_PROP);
		if (storedType && storedType['@id'] !== typeId) throw new Error('want to init with type '+type+' but is stored as type '+storedType['@id']);
		metaNode[BaseRepo.GRAPH_TYPE_PROP] = { '@id': typeId };

		let storedName = metaNode.getOne(BaseRepo.GRAPH_NAME_PROP);
		if (opts.graphName) {
			if (storedName && storedName['@id'] !== graphName) throw new Error('expected graph to be name '+graphName+' but is stored as '+storedName['@id']);
			metaNode[BaseRepo.GRAPH_NAME_PROP] = { '@id': graphName };
		} else if (!metaNode[BaseRepo.GRAPH_NAME_PROP]) {
			metaNode[BaseRepo.GRAPH_NAME_PROP] = { '@id': graphName };
		}

		if (opts.tags) {
			for (let tag of opts.tags) {
				metaNode.addToProp(BaseRepo.GRAPH_TAG_PROP, new Literal({ '@value': tag }))
			}
		}

		const storageOpts: BaseStorageOptions = {
			id,
			graphName: metaNode[BaseRepo.GRAPH_NAME_PROP]['@id']
		}

		let storage;
		switch (type) {
			case 'kv': {
				storage = new KeyValueStorage(this, storageOpts)
				break
			}
			case 'doc': {
				storage = new DocumentStorage(this, storageOpts)
				break;
			}
			case 'graph': {
				storage = new GraphStorage(this, storageOpts);
				break
			}
			default: {
				throw new Error('unknown type: '+type);
			}
		}
		this.loadedStorageMap.set(opts.id, storage);
		this.loadedStorageMap.set(id, storage);
		this.updateMetaRepo();
		return storage;
	}

	protected savePromise: ExposedPromise<Graph>
	protected saveTimer: any

	protected updateMetaRepo(): ExposedPromise<Graph> {
		if (!this.savePromise) {
			let p: any = new Promise((res, rej) => {
				setImmediate(() => {
					p.resolve = res;
                	p.reject = rej;
				})
			});
			this.savePromise = p;
		}
		if (this.saveTimer) {
			clearTimeout(this.saveTimer);
		}
		this.saveTimer = setTimeout(() => this.updateMetaRepoInternal(), 100);
		return this.savePromise;
	}

	protected async updateMetaRepoInternal(): Promise<void> {
		try {
			let doc = await this.metaRepoInit;
			await this.metaRepoStorage.persistGraph(doc);
			if (this.savePromise) this.savePromise.resolve(doc);
		} catch (err) {
			console.error(err);
			if (this.savePromise) this.savePromise.reject(err);
			else throw err;
		}
		this.savePromise = null;
	}

	public async getMetaRepo(): Promise<Graph> {
		if (this.metaRepoInit) return this.metaRepoInit;
		this.metaRepoInit = this.initMetaRepoInternal();
		return this.metaRepoInit;
	}

	protected async initMetaRepoInternal(): Promise<Graph> {
		if (!this.metaRepoStorage) {
			this.metaRepoStorage = new GraphStorage(this, { id: BaseRepo.META_STORAGE_NAME, graphName: BaseStorage.GRAPH_PREFIX + BaseRepo.META_STORAGE_NAME });
		}
		let graph = await this.metaRepoStorage.load(this.metaRepoStorage.createDescribeQuery())
		return graph;
	}

	public async listStorages(filter?: ListStorageFilter): Promise<BaseStorage[]> {
		let tagFilter = filter && filter.tags && filter.tags.length ? filter.tags : null;
		let metaGraph = await this.getMetaRepo();
		let res: BaseStorage[] = [];
		for (let node of metaGraph.getGraph()) {
			let idUri = node.getId();
			let id = idUri.substring(BaseStorage.GRAPH_PREFIX.length);
			if (tagFilter) {
				let tags: Literal[] = node.getAll(BaseRepo.GRAPH_TAG_PROP);
				let match = false;
				for (let tag of tagFilter) {
					let has = !!tags.find(t => t.getValue() === tag);
					if (!has) {
						match = false;
						break;
					}
					match = true;
				}
				if (!match) continue;
			}
			if (this.loadedStorageMap.has(id)) {
				res.push(this.getInitializedStorage(id))
			} else {
				let typeUri = node.getOne(BaseRepo.GRAPH_TYPE_PROP).getId()
				let type = typeUri.substring(BaseRepo.GRAPH_TYPE_PREFIX.length);
				let storage = await this.initStorage(type, { id })
				res.push(storage);
			}
		}
		return res;
	}

	public async deleteStorage(id: string) {
		let metaGraph = await this.getMetaRepo();
		const idUri = BaseStorage.GRAPH_PREFIX + id;
		let node = metaGraph.getNodeById(idUri);
		node.destroy();
		this.loadedStorageMap.delete(id);
		this.updateMetaRepo();
	}

}
