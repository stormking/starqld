import BaseRepo from './base-repo.js'
import * as QueryBuilder from './qb/main.js'
import DescribeBuilder from './qb/describe.js'
import { fetch, Request } from '../util/fetch.js'
import { UploadResult } from '../types.js'
import Doc from '../model/graph.js'
import Node from '../model/node.js'
import SelectBuilder from './qb/select.js'
import AskBuilder from './qb/ask.js'
import BaseStorage from './storage/base.js'
import KeyValueStorage from './storage/keyvalue.js'
import DocumentStorage from './storage/document.js'
import GraphStorage from './storage/graph.js'
import MultiGraphStorage from './storage/multigraph.js'
import { sleep } from '../util/util.js'

export type SelectResult = {
	head: {
		vars: string[]
	}
	results: {
		bindings: SelectBinding[]
	}
}
export type SelectBinding = {
	[key: string]: {
		type: 'uri'|'literal',
		value: string,

	}
}

export default class FusekiRepo extends BaseRepo {

	private uri: URL
	private auth = ''

	constructor(url: string) {
		super();
		this.uri = new URL(url)
		if (this.uri.username) {
			this.auth = btoa(`${this.uri.username}:${this.uri.password}`)
			this.uri.username = '';
			this.uri.password = '';
		}
		if (!this.getCollectionName()) {
			throw new Error('missing collection name in '+url)
		}
	}

	getCollectionName(): string {
		return this.uri.pathname.substring(1);
	}

	query(query: string, acceptMime?: string): Promise<unknown> {
		return this.request('POST', this.uri.href+'/query', query, 'application/sparql-query', acceptMime)
	}

	update(query: string): Promise<unknown> {
		return this.request('POST', this.uri.href+'/update', query, 'application/sparql-update')
	}

	upload(jsonLd: string): Promise<UploadResult> {
		return this.request('POST', this.uri.href+'/data', jsonLd, 'application/ld+json')
	}

	uploadTurtle(jsonLd: string): Promise<UploadResult> {
		return this.request('POST', this.uri.href+'/data', jsonLd, 'application/trig') //'text/turtle'
	}

	async assertCollection() {
		let collection = this.getCollectionName();
		let baseUri = new URL(this.uri.href);
		baseUri.pathname = '/$/ping'
		let up = await this.request('GET', baseUri.href, null)
		if (!up) throw new Error('fuseki server is not up');
		baseUri.pathname = '/$/datasets';
		let exists = await this.collectionExists(collection);
		if (exists) return;
		await this.request('POST', baseUri.href, 'dbName='+collection+'&dbType=tdb2')
	}

	async destroyCollection() {
		let collection = this.getCollectionName();
		let exists = await this.collectionExists(collection);
		if (!exists) return;
		let baseUri = new URL(this.uri.href);
		baseUri.pathname = '/$/datasets/'+collection;
		try {
			await this.request('DELETE', baseUri.href, null);
		} catch (e) {
			if (e.message.includes('Active transactions')) {
				await sleep(100);
				return this.destroyCollection();
			} else {
				throw e;
			}
		}
	}

	async collectionExists(name: string): Promise<boolean> {
		let baseUri = new URL(this.uri.href);
		baseUri.pathname = '/$/datasets';
		let sets = await this.request('GET', baseUri.href, null);
		let found = sets.datasets.find(e => e['ds.state'] === true && e['ds.name'] === '/'+name);
		return !!found;
	}


	async request(method: string, url: string, params: any, contentMime?: string, acceptMime?: string) {
		let body: string;
		const info: RequestInit = {
			cache: 'no-cache',
			method,
			body: null,
			headers: {
				Accept: acceptMime || 'application/ld+json'
			}
		};
		if (params) {
			if (typeof params !== 'string') {
				body = JSON.stringify(params)
				info.headers['Content-Type'] = 'application/json; charset=UTF-8'
			} else {
				body = params;
				info.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
			}
			// console.log(body);
			info.body = body;
		}
		if (this.auth) {
			info.headers['Authorization'] = 'Basic '+this.auth
		}
		if (contentMime) {
			info.headers['Content-Type'] = contentMime
		}
		// debuglog(url, info);
		// console.log(info);
		const req = new Request(url, info);
		const res = await fetch(req);
		if (![200, 204].includes(res.status)) {
			let output = await res.text();
			// console.log(output, res);
			throw new Error('HTTP Error '+res.status+': '+output);
		}
		let ct = res.headers.get('content-type');
		if (ct && ct.includes('json')) {
			return res.json();
		} else {
			return res.text();
		}
	}

	async select(qb: SelectBuilder|string): Promise<SelectBinding[]> {
		let q: string;
		if (qb instanceof SelectBuilder) {
			q = qb.toString();
		} else {
			q = qb;
		}
		const res: any = await this.query(q, 'application/json');
		return res.results.bindings;
	}

	async ask(qb: AskBuilder|string): Promise<boolean> {
		let q: string;
		if (qb instanceof AskBuilder) {
			q = qb.toString();
		} else {
			q = qb;
		}
		const res: any = await this.query(q, 'application/json');
		return res.boolean;
	}

	async *iterateOverSelect(qb: SelectBuilder): AsyncGenerator {
		const batchSize = 500;
		let start = 0;
		qb.setLimit(batchSize);
		qb.setOffset(start);
		while (true) {
			let q = qb.toString();
			const res: any = await this.query(q, 'application/json');
			// console.log(res);
			for (let row of res.results.bindings) {
				yield row;
			}
			if (res.results.bindings.length < batchSize) break;
			start += batchSize;
			qb.setOffset(start);
		}
	}

	

}
