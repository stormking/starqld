import QueryBuilder from "./base.js";

export default class AskBuilder extends QueryBuilder {

	protected rawConditions: string[] = [];

	addRawCondition(cond: string) {
		this.rawConditions.push(cond);
	}


	toString(): string {
		let conditions = '';
		if (this.rawConditions.length) {
			for (let cond of this.rawConditions) {
				conditions += `${cond} .`;
			}
		}
		if (!conditions) {
			throw new Error('no conditions found');
		}
		let prefixes = this._getPrefixStatement();
		let res = `${prefixes}\n\nASK { ${conditions} }`;
		// debug('describe-query: ', res);
		return res;
	}

}
