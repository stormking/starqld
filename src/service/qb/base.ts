import DescribeBuilder from "./describe.js";
import { PrefixDef } from "../../types.js";

export type Triplet = [string, string, string]
export type OptionalBlock = Triplet[]

export default abstract class QueryBuilder {

	protected prefixes: PrefixDef[] = []
	protected limit = 100000;
	protected offset = 0;
	protected namedGraphs: string[] = []

	protected rawConditions: string[] = [];
	protected conditions: Triplet[] = [];
	protected optionals: OptionalBlock[] = [];
	protected selects: Triplet[] = [];

	protected variables = new Map();
	protected preparedToSave = false;

	abstract toString();

	setLimit(l: number) {
		this.limit = l;
	}

	setOffset(o: number) {
		this.offset = o;
	}

	addPrefixes(list: PrefixDef[]) {
		this.prefixes = this.prefixes.concat(list);
	}

	setNamedGraphs(g: string[]) {
		this.namedGraphs = g;
	}

	protected _getPrefixStatement(): string {
		return this.prefixes.map(({ as, uri }) => `PREFIX ${as}: <${uri}>`).join('\n')
	}

	protected resolveOnlyVar(v: string) {
		if (this.variables.has(v)) {
			return this.variables.get(v);
		}
		throw new Error('no a var: '+v);
	}

	protected resolveVar(v: string) {
		if (this.variables.has(v)) {
			return this.variables.get(v);
		}
		if (v.startsWith('http://')) {
			return `<${v}>`
		}
		return v;	//should be prefixed uri
	}

	addCondition(a: string, b: string, c: string) {
		this.conditions.push([
			this.resolveVar(a),
			this.resolveVar(b),
			this.resolveVar(c),
		])
	}

	addRawCondition(cond: string) {
		this.rawConditions.push(cond);
	}

	addOptionalBlock(block: OptionalBlock) {
		this.optionals.push(block.map(([a, b, c]) => {
			return [
				this.resolveVar(a),
			this.resolveVar(b),
			this.resolveVar(c),
			]
		}));
	}

	getVar(role: string) {
		if (!this.variables.has(role)) throw new Error('role not set: '+role);
		return this.variables.get(role);
	}

	addVar(role: string) {
		if (this.variables.has(role)) throw new Error('role already set: '+role);
		let name = `?v${this.variables.size}`;
		this.variables.set(role, name);
	}

	buildWhere(): string {
		return this.conditions.map(a => a.join(' ')).join('.\n') + (this.conditions.length ? ' .\n' : '')
		+ this.rawConditions.join('\n') + '\n' 
		+ this.optionals.map(o => ` OPTIONAL { ${o.map(c => c.join(' ')).join('.\n')} }.\n`).join('\n') + '\n'
	}

	protected prepSave() {
		
	}

	static objectSerializeForQuery(o: any): string {
		if (o && o.getId) {
			return `<${o.getId()}>`
		}
		if (o && o.substring && o.substring(0, 4) === 'http') {
			return `<${o}>`
		}
		return `${o}`
	}

}

