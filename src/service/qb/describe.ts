import ChildNode from "../../model/child-node.js";
import QueryBuilder, { Triplet } from "./base.js";


export default class DescribeBuilder extends QueryBuilder {

	static MAIN_SUB = 'MainSubject';
	static MAIN_PRED = 'MainPredicate';
	static MAIN_OBJ = 'MainObject';
	static CHILD_SUB = 'ChildSubject';
	static CHILD_PRED = 'ChildPredicate';
	static CHILD_OBJ = 'ChildObject';
	static REL_SUB = 'RelSubject';
	static REL_PRED = 'RelPredicate';
	static REL_OBJ = 'RelObject';
	static OBJ_PRED = 'ObjPred';
	static OBJ_OBJ = 'ObjObj';
	

	protected idFilter: string[] = [];
	protected selects: Triplet[] = [];
	protected relatedProps: string[] = [];
	protected includeChildNodes = true;
	protected includeRelatedNodes = false;

	constructor() {
		super();
		this.addVar(DescribeBuilder.MAIN_SUB);
		this.addVar(DescribeBuilder.MAIN_PRED);
		this.addVar(DescribeBuilder.MAIN_OBJ);
		this.addVar(DescribeBuilder.CHILD_SUB);
		this.addVar(DescribeBuilder.CHILD_PRED);
		this.addVar(DescribeBuilder.CHILD_OBJ);
		this.addVar(DescribeBuilder.REL_SUB);
		this.addVar(DescribeBuilder.REL_PRED);
		this.addVar(DescribeBuilder.REL_OBJ);
		this.addVar(DescribeBuilder.OBJ_PRED);
		this.addVar(DescribeBuilder.OBJ_OBJ)
		this.addCondition(DescribeBuilder.MAIN_SUB, DescribeBuilder.MAIN_PRED, DescribeBuilder.MAIN_OBJ);
		this.selects.push([
			this.getVar(DescribeBuilder.MAIN_SUB),
			this.getVar(DescribeBuilder.MAIN_PRED),
			this.getVar(DescribeBuilder.MAIN_OBJ)
		])
	}

	addSelect(a: string, b: string, c: string) {
		this.selects.push([
			this.resolveVar(a),
			this.resolveVar(b),
			this.resolveVar(c),
		])
	}


	addIdToFilter(id: string) {
		this.idFilter.push(id);
	}

	setIncludeChildNodes(b: boolean) {
		this.includeChildNodes = b;
	}

	setIncludeRelatedNodes(b: boolean) {
		this.includeRelatedNodes = b;
	}
	

	addRelatedProperty(p: string) {
		this.relatedProps.push(p);
	}

	
	protected prepSave() {
		if (this.preparedToSave) return;
		this.preparedToSave = true;
		super.prepSave();
		if (this.includeChildNodes) {
			this.optionals.push([
				[
					this.getVar(DescribeBuilder.CHILD_SUB),
					'<'+ChildNode.PARENT_PROP+'>',
					this.getVar(DescribeBuilder.MAIN_SUB)
				],
				[
					this.getVar(DescribeBuilder.CHILD_SUB),
					this.getVar(DescribeBuilder.CHILD_PRED),
					this.getVar(DescribeBuilder.CHILD_OBJ)
				]
			])
			this.selects.push([
				this.getVar(DescribeBuilder.CHILD_SUB),
				this.getVar(DescribeBuilder.CHILD_PRED),
				this.getVar(DescribeBuilder.CHILD_OBJ)
			])
		}
		if (this.includeRelatedNodes) {
			this.optionals.push([
				[
					this.getVar(DescribeBuilder.REL_SUB),
					this.getVar(DescribeBuilder.REL_PRED),
					this.getVar(DescribeBuilder.MAIN_SUB)
				]
			])
			this.selects.push([
				this.getVar(DescribeBuilder.REL_SUB),
				this.getVar(DescribeBuilder.REL_PRED),
				this.getVar(DescribeBuilder.MAIN_SUB)
			])
		}
		if (this.relatedProps.length) {
			this.selects.push([
				this.getVar(DescribeBuilder.MAIN_OBJ),
				this.getVar(DescribeBuilder.OBJ_PRED),
				this.getVar(DescribeBuilder.OBJ_OBJ)
			]);
			this.rawConditions.push(
				`OPTIONAL { 
					${this.getVar(DescribeBuilder.MAIN_OBJ)} ${this.getVar(DescribeBuilder.OBJ_PRED)} ${this.getVar(DescribeBuilder.OBJ_OBJ)} . 
					VALUES ${this.getVar(DescribeBuilder.OBJ_PRED)} { ${this.relatedProps.map(QueryBuilder.objectSerializeForQuery).join(' ')}}. 
				} .\n`
			)
		}
		if (this.idFilter.length) {
			this.rawConditions.push(
				` VALUES ${this.getVar(DescribeBuilder.MAIN_SUB)} {
					${this.idFilter.map(QueryBuilder.objectSerializeForQuery).join(' ')}
				} .\n`
			);
		}
	}

	toString(): string {
		this.prepSave();
		
		let where = this.buildWhere();
		let prefixes = this._getPrefixStatement();
		let from = '';
		if (this.namedGraphs.length) {
			from = this.namedGraphs.map(g => `FROM <${g}>`).join('\n')
		}
		let select = this.selects.map(e => e.join(' ')).join(' . ') + ' .\n';
		let res = `${prefixes}
		CONSTRUCT { 
			${select} 
		} 
		${from} 
		WHERE {
			${where} 
		}
		OFFSET ${this.offset}
		LIMIT ${this.limit}`;
		// console.debug('describe-query: ', res);
		return res;
	}

}
