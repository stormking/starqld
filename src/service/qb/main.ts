import AskBuilder from "./ask.js";
import DescribeBuilder from "./describe.js";
import SelectBuilder from "./select.js";

export const DESCRIBE = 'describe';
export const SELECT = 'select';
export const ASK = 'ask';

export function create(type: string): any {
	switch (type) {
		default: {
			throw new Error('unknown type: '+type);
		}
		case DESCRIBE: {
			return new DescribeBuilder();
		}

		case SELECT: {
			return new SelectBuilder();
		}

		case ASK: {
			return new AskBuilder();
		}
	}
}

