import QueryBuilder from "./base.js";

export default class SelectBuilder extends QueryBuilder {

	static TARGET = 'target';

	protected targets: string[] = [];
	protected groupBy: string[] = [];

	constructor() {
		super();
		this.addVar(SelectBuilder.TARGET);
		this.targets.push(this.resolveVar(SelectBuilder.TARGET));
	}

	setSelectTargets(x: string[]) {
		this.targets = x.map(e => this.resolveVar(e))
	}

	setSelectAll() {
		this.targets = ['*'];
	}

	setGroupBy(x: string[]) {
		this.groupBy = x.map(e => this.resolveVar(e))
	}

	protected _getGroup(): string {
		return this.groupBy.join(' ');
	}

	toString(): string {
		this.prepSave();
		let from = '';
		if (this.namedGraphs.length) {
			from = this.namedGraphs.map(g => `FROM <${g}>`).join(' ')
		}
		let prefixes = this._getPrefixStatement();
		let group = this._getGroup();
		let res = `${prefixes}
		SELECT ${this.targets.join(' ')}
		${from} 
		WHERE {
			${this.buildWhere()}
		}
		${group ? `GROUP BY ${group}` : ''}
		OFFSET ${this.offset} 
		LIMIT ${this.limit}`;
		// console.debug('select-query: ', res);
		return res;
	}

}
