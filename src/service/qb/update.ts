import QueryBuilder from "./base.js";

export default class UpdateBuilder extends QueryBuilder {

	#graphName: string

	protected wrapGraphName(inner: string): string {
		if (!this.#graphName) return inner;
		return `GRAPH <${this.#graphName}> { ${inner} }`;
	}

	toString(): string {
		let conditions = '';
		return '';
	}

}
