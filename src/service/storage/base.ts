import type BaseRepo from "../base-repo.js"

export type BaseStorageOptions = {
    id: string,
    graphName: string,
}

export default abstract class BaseStorage {

    static GRAPH_PREFIX = 'http://starql.org/owl/graph/name/'
    protected keyValid = /[a-z0-9_-]{1,128}/i

    #repo: BaseRepo
    #id: string
    #graphName: string


    constructor(repo: BaseRepo, opts: BaseStorageOptions) {
        this.#repo = repo;
        this.#id = opts.id;
        this.#graphName = opts.graphName;
        // if (!this.keyValid.test(this.#id)) throw new Error('not a valid id: "'+this.#id+'"')
    }

    public getRepo(): BaseRepo {
        return this.#repo;
    }

    public getGraphName(): string {
        return this.#graphName;
    }

	public async dropGraph() {
		await this.getRepo().update(`DROP GRAPH <${this.getGraphName()}>`)
	}
    
}
