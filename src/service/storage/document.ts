import Doc from "../../model/doc.js";
import BaseRepo from "../base-repo.js";
import BaseStorage from "./base.js";
import DescribeBuilder from '../qb/describe.js'


export default class DocumentStorage extends BaseStorage {

	PREFIX_BLOCK = 'PREFIX json: <http://starqld.org/owl/doc/json/> \nPREFIX xsd:  <http://www.w3.org/2001/XMLSchema#> \n'
	PREFIX_LIST = [
		{ as: 'json', uri: 'http://starqld.org/owl/doc/json/' },
		{ as: 'xsd', uri: 'http://www.w3.org/2001/XMLSchema#' }
	];

    public async load(id: string): Promise<Doc> {
        let qb = this.getRepo().createDescribeQuery()
		qb.addPrefixes(this.PREFIX_LIST)
        let idUri = this.getIdUri(id);
        qb.addIdToFilter(idUri);
        qb.setNamedGraphs([this.getGraphName()])
        // console.log(qb.toString());
        let res: any = await this.getRepo().query(qb.toString());
		// console.log('load', res);
		if (!res) {
			throw new Error('not found')
		}
		if ('@graph' in res) {
			throw new Error('found more than 1 result for id: '+id);
		}
		if (!('@id' in res)) {
			throw new Error('found no doc for id '+id);
		}
        return Doc.fromStorage(res);
    }

    public async find(key: string, value: string): Promise<Doc[]> {
        let qb = this.getRepo().createDescribeQuery();
		qb.addPrefixes(this.PREFIX_LIST)
        qb.setNamedGraphs([this.getGraphName()])
		qb.addCondition(DescribeBuilder.MAIN_SUB, Doc.PATH_PREFIX+key, JSON.stringify(value));
        // qb.addRawCondition(` ${JSON.stringify(key)} ${JSON.stringify(value)}`);
        let res: any = await this.getRepo().query(qb.toString());
        let list = '@graph' in res ? res['@graph'] : '@id' in res ? [res] : [];
        return list.map(d => Doc.fromStorage(d));
    }

    public async loadAll(): Promise<Doc[]> {
        let qb = this.getRepo().createDescribeQuery()
		qb.addPrefixes(this.PREFIX_LIST)
        qb.setNamedGraphs([this.getGraphName()])
        let res: any = await this.getRepo().query(qb.toString());
		// console.log('loadAll', res);
        let list = '@graph' in res ? res['@graph'] : '@id' in res ? [res] : [];
        return list.map(d => Doc.fromStorage(d));
    }

    public async store(doc: Doc): Promise<boolean> {
        let changes = doc.getChanges();
        doc.increaseRev();
        const graph = this.getGraphName();
        let q = this.PREFIX_BLOCK;
        function wrapGraphName(inner: string) {
			return `GRAPH <${graph}> { ${inner} }`;
		}
		if (changes.removed && changes.removed.length) {
			q += `DELETE DATA { ${wrapGraphName(changes.removed.join('\n'))} }; `;
		}
		if (changes.added && changes.added.length) {
            q += `INSERT DATA { ${wrapGraphName(changes.added.join('\n'))} }; `;
        }
        if (!q.length) return false;
		// console.debug('query', q);
		await this.getRepo().update(q)
		doc.lockInitialData();
        return true;
    }

    public async storeData(data: Record<string, any>): Promise<Doc> {
        let doc = new Doc();
        doc.initNew();
        doc.setData(data);
        await this.store(doc);
        return doc;
    }

	public async delete(id: string) {
		let doc = await this.load(id);
		let removal = doc.getChangesToDelete();
		const graph = this.getGraphName();
        let q = this.PREFIX_BLOCK;
        function wrapGraphName(inner: string) {
			return `GRAPH <${graph}> { ${inner} }`;
		}
		q += `DELETE DATA { ${wrapGraphName(removal.join('\n'))} }; `;
        if (!q.length) return false;
		// console.debug('query', q);
		await this.getRepo().update(q);
	}

    protected getIdUri(id: string): string {
		if (id.startsWith('http://')) return id;
        if (!this.keyValid.test(id)) throw new Error('not a valid id: "'+id+'"')
        return Doc.META_PREFIX+'id#'+id;
    }

}
