import DescribeBuilder from "../qb/describe.js";
import BaseStorage from "./base.js";
import Graph, { LoadMap, LoadMapFn } from '../../model/graph.js';
import Node from '../../model/node.js';
import BaseRepo from "../base-repo.js";
import { SelectResult } from "../fuseki.js";

type PersistOpts = {
	dryRun?: boolean,
	ifNotExists?: boolean,
    create?: boolean,
	skipChildNodes?: boolean
}

export default class GraphStorage extends BaseStorage {


    async load(qb: DescribeBuilder, loadMap?:LoadMap|LoadMapFn ): Promise<Graph> {
        const res: any = await this.getRepo().query(qb.toString());
		// console.log(JSON.stringify(res, null, 2))
		return Graph.createNormalized(res, loadMap);
    }

    createDescribeQuery() {
        let qb = this.getRepo().createDescribeQuery();
		qb.setNamedGraphs([ this.getGraphName() ])
		return qb;
    }

	createSelectQuery() {
		let qb = this.getRepo().createSelectQuery();
		qb.setNamedGraphs([ this.getGraphName() ]);
		return qb;
	}

    async persistGraph(graph: Graph, opts?: PersistOpts) {
        if (!opts) opts = { dryRun: false, ifNotExists: false, create: false, skipChildNodes: true };
		else opts.skipChildNodes = true;
        let all = graph.getGraph().map((doc) => this.getChanges(doc, opts))
        let res = await Promise.all(all)
        let q = res.join('\n')
        // console.log(q);
		if (q.trim().length === 0) return;
        if (!opts.dryRun) await this.getRepo().update(q)
		else console.log('dryRun query: ', q)
        graph.getGraph().forEach(doc => doc.lockInitialData())
    }

    async persistDoc(doc: Node, opts?: PersistOpts) {
		if (!opts) opts = { dryRun: false, ifNotExists: false };
		let q = await this.getChanges(doc, opts);
		if (!opts.dryRun) await this.getRepo().update(q)
		else console.log('dryRun query: ', q)
		doc.lockInitialData();
	}

    protected async getChanges(doc: Node, opts?: PersistOpts): Promise<string> {
        let changes = await doc.getChanges(opts.create, opts.skipChildNodes);
		let q = '';
		if (changes.removed && changes.removed.length) {
			q += `DELETE DATA {\n${this.wrapGraphName(changes.removed.join('\n'))}\n}; `;
		}
		if (opts.ifNotExists) {
			if (changes.added && changes.added.length) {
				let toAdd = [];
				for (let add of changes.added) {
					let exists: any = await this.getRepo().query(`ASK { ${this.wrapGraphName(add)} }`, 'application/json');
					if (!exists.boolean) {
						toAdd.push(add);
					}
				}
				if (toAdd.length) {
					q += `INSERT DATA { ${this.wrapGraphName(toAdd.join('\n'))} }; `;
				}
			}
		} else {
			if (changes.added && changes.added.length) {
				q += `INSERT DATA { ${this.wrapGraphName(changes.added.join('\n'))} }; `;
			}
		}
		// console.debug('getChanges', q);
        return q;
    }

	async removeDoc(n: Node, opts: { includeReferences?: boolean } = {}) {
		const includeReferences = !!opts.includeReferences;
		const id = n.getId();
		n.lockInitialData();
		n.replaceData({})
		delete n['@type']
		await this.persistDoc(n, { dryRun: false });
		if (includeReferences) {
			let select = `SELECT ?s ?p ?o FROM <${this.getGraphName()}> WHERE { ?s ?p <${id}> }`;
			let selectRes: SelectResult = await this.getRepo().query(select, 'application/json') as any;
			// console.dir(selectRes, { depth: 99 });
			let remove = selectRes.results.bindings.map(b => `<${b.s.value}> <${b.p.value}> <${id}> .`);
			let q = `DELETE DATA {\n${this.wrapGraphName(remove.join('\n'))}\n}; `;
			await this.getRepo().update(q);
		}
	}


	protected wrapGraphName(inner: string) {
		return `GRAPH <${this.getGraphName()}> {\n${inner}\n}`;
	}

}
