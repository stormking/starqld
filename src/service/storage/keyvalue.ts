import BaseRepo from "../base-repo.js";
import SelectBuilder from "../qb/select.js";
import BaseStorage, { BaseStorageOptions } from "./base.js";


export default class KeyValueStorage extends BaseStorage {

    static KEY_PREFIX = 'http://starqld.org/owl/kv/key/';
    static VALUE = 'http://starqld.org/owl/kv/value';
    static EXPIRES = 'http://starqld.org/owl/kv/expires'
    static TAGGED = 'http://starqld.org/owl/kv/tagged'
    static TAG_PREFIX = 'http://starqld.org/owl/kv/tag/'

    #keyValid = /[a-z0-9_-]{1,128}/i

    public async set(key: string, value: any, expires?: number, tags?: string[]) {
        await this.unset(key);
        const keyUri = this.getKeyUri(key);
        const valueTriplet = `<${keyUri}> <${KeyValueStorage.VALUE}> '${JSON.stringify(value)}' .`;
        const expiresTriplet = expires ? `<${keyUri}> <${KeyValueStorage.EXPIRES}> ${Math.floor(Date.now()/1000+expires)} .` : '';
        const tagTriplets = tags ? tags.map(tag => `<${keyUri}> <${KeyValueStorage.TAGGED}> <${this.getTagUri(tag)}> .`).join('\n') : '';
        let query = `
            INSERT DATA { GRAPH <${this.getGraphName()}> {
                ${valueTriplet}
                ${expiresTriplet}
                ${tagTriplets}
            } }
        `;
        let res = await this.getRepo().update(query);
        // console.log('set', res);
    }

    public async listEntriesByTag(tag: string) {
        let qb = this.getRepo().createSelectQuery();
		qb.addVar('x');
		qb.addVar('p');
		qb.addVar('o');
        qb.addRawCondition(`
            ${qb.getVar('x')} <${KeyValueStorage.TAGGED}> <${this.getTagUri(tag)}> . 
            ${qb.getVar('x')} ${qb.getVar('p')} ${qb.getVar('o')} . 
            VALUES ${qb.getVar('p')} { <${KeyValueStorage.VALUE}> <${KeyValueStorage.EXPIRES}> }
        `);
        qb.setSelectTargets(['x', 'p', 'o']);
        qb.setLimit(10000);
        qb.setNamedGraphs([this.getGraphName()]);
        let res: any = await this.getRepo().query(qb.toString(), 'application/json')
        // console.log('list', res.results.bindings);
        const rows = res.results.bindings;
        const list = [];
        const now = Math.floor(Date.now()/1000);
		let propP = qb.getVar('p').substring(1);
		let propX = qb.getVar('x').substring(1);
		let propO = qb.getVar('o').substring(1);
        for (let row of rows) {
            if (row[propP].value !== KeyValueStorage.VALUE) continue;
            const keyUri = row[propX].value;
            let expires = rows.find(r => r[propP].value === KeyValueStorage.EXPIRES && row[propX].value === keyUri);
            if (expires) {
                let expTime = ~~expires[propO].value;
                if (expTime < now) {
                    continue;
                }
            }
            const value = JSON.parse(row[propO].value);
            const key = keyUri.substring(KeyValueStorage.KEY_PREFIX.length)
            list.push([key, value])
        }
        return list;
    }

    public async unset(key: string) {
        let query = `DELETE WHERE { GRAPH <${this.getGraphName()}> { <${this.getKeyUri(key)}> ?p ?o . } }`;
        let res = await this.getRepo().update(query);
        // console.log('deleted', res);
    }

    public async get(key: string, defaultValue?: any) {
        let qb = this.getRepo().createSelectQuery();
        qb.addRawCondition(`<${this.getKeyUri(key)}> ?p ?o`);
        qb.setSelectAll();
        qb.setNamedGraphs([this.getGraphName()]);
        let res: any = await this.getRepo().query(qb.toString(), 'application/json')
        // console.log('get', res.results.bindings);
        const rows = res.results.bindings;
        let expires = rows.find(r => r.p.value === KeyValueStorage.EXPIRES);
        if (expires) {
            let expTime = ~~expires.o.value;
            if (expTime < Math.floor(Date.now()/1000)) {
                return defaultValue;
            }
        }
        let valueRow = rows.find(r => r.p.value === KeyValueStorage.VALUE);
        if (!valueRow) return defaultValue;
        return JSON.parse(valueRow.o.value);
    }

    protected getKeyUri(key: string): string {
        if (!this.#keyValid.test(key)) throw new Error('not a valid key: "'+key+'"')
        return KeyValueStorage.KEY_PREFIX+key;
    }

    protected getTagUri(tag: string): string {
        if (!this.#keyValid.test(tag)) throw new Error('not a valid tag: "'+tag+'"')
        return KeyValueStorage.TAG_PREFIX+tag;
    }

}
