import DescribeBuilder from "../qb/describe.js";
import BaseStorage from "./base.js";
import Graph from '../../model/graph.js';
import BaseRepo from "../base-repo.js";

export default class MultiGraphStorage extends BaseStorage {


    #graphNames: string[]

    constructor(repo: BaseRepo, graphNames: string[]) {
        super(repo, { graphName: '', id: 'multimeta' });
		if (graphNames.length === 0) throw new Error('must have at least one graphName');
        this.#graphNames = graphNames;
    }

    async load(qb: DescribeBuilder): Promise<Graph> {
        qb.setNamedGraphs(this.getGraphNames())
        const res: any = await this.getRepo().query(qb.toString());
		// console.log(JSON.stringify(res, null, 2))
		return Graph.createNormalized(res);
    }

    protected getGraphNames(): string[] {
        return this.#graphNames;
    }

    createDescribeQuery() {
        return this.getRepo().createDescribeQuery();
    }

	createSelectQuery() {
		let qb = this.getRepo().createSelectQuery();
		qb.setNamedGraphs(this.getGraphNames());
		return qb;
	}

}
