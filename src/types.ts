

export type UploadResult = {
	count: number,
	tripleCount: number,
	quadCount: number
}

export type PrefixDef = {
	uri: string,
	as: string
}
