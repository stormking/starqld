import jsonld from 'jsonld';

const ld: any = jsonld;
const oldLoader = ld._documentLoader;
const cache = new Map();

ld.documentLoader = documentLoader;

export async function documentLoader(url: string): Promise<any> {
	// console.log('load url', url);
	if (!cache.has(url)) {
		// console.log('loading fresh', url);
		try {
			let res = await oldLoader(url);
			cache.set(url, res);
		} catch (e) {
			cache.set(url, { error: e });
		}
	}
	let res = cache.get(url);
	if (res.error) throw res.error;
	return res;
}
