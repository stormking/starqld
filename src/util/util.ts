type Key = string|number|Symbol
type Visitor = (val: any, key: Key, stack: Key[]) => any;
type TraversalOpts = {
    depthFirst?: boolean,
    change?: boolean
}

export function traverse(target: any, visitor: Visitor, opts?: TraversalOpts, stack?: any[]) {
    if (!stack) stack = [];
    if (!opts) opts = {};
    let ret;
    if (Array.isArray(target)) {
        ret = [];
        for (let i=0, ii=target.length; i<ii; i++) {
            let value = target[i];
            let newStack = stack.slice(0);
            let r;
            newStack.push(i);
            if (!opts.depthFirst) {
                let newValue = visitor(value, i, newStack)
                if (opts.change) {
                    if (typeof newValue !== 'undefined') {
                        value = newValue;
                        target[i] = newValue;
                    } else {
                        target.splice(i, 1)
                    }
                }
                
            }
            r = traverse(value, visitor, opts, newStack);
            if (opts.depthFirst) {
                let newValue = visitor(value, i, newStack)
                if (typeof newValue !== 'undefined') value = newValue;
            }
            ret.push(r);
        }
    } else if (target && typeof target === 'object') {
        ret = {}
        for (let [k, v] of Object.entries(target)) {
            let newStack = stack.slice(0);
            newStack.push(k);
            if (!opts.depthFirst) {
                let newValue = visitor(v, k, newStack)
                if (opts.change) {
                    if (typeof newValue !== 'undefined') {
                        ret[k] = v = newValue;
                    } else {
                        continue;
                    }
                }
                
            }
            traverse(v, visitor, opts, newStack)
            if (opts.depthFirst) {
                visitor(v, k, newStack)
            }
        }
    }
    if (stack.length === 0) {
        let newValue = visitor(target, null, stack)
        if (opts.change) {

        }
    }
    return ret;
}

export function sleep(ms: number) {
	return new Promise(res => {
		setTimeout(res, ms)
	})
}
