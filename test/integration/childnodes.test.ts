import { describe, it, before, after } from 'mocha'
import { expect } from 'chai'
import { FusekiRepo, Graph, Node } from '../../src/index.js';
import FS from 'fs';
import GraphStorage from '../../src/service/storage/graph.js';
import ChildNode from '../../src/model/child-node.js';

describe('Child Nodes', async() => {
	
	describe('Basics', () => {
		let repo: FusekiRepo
		let ent1Json: string
		let storage : GraphStorage;
		const parentProp = 'http://example.org/librarian';
		const ageProp = 'http://example.org/age';
		const nameProp = 'http://example.org/name';
		const parentId = 'http://example.org/library';
		let childId = '';
		before(async() => {
			repo = new FusekiRepo('http://admin:admin@localhost:3030/test1');
			await repo.destroyCollection();
			await repo.assertCollection();
			storage = await repo.assertGraphStorage('test2');
			ent1Json = await FS.promises.readFile('./test/fixtures/library.jsonld', { encoding: 'utf8' })
		})
	
		it('can insert some data', async() => {
			let graph = await Graph.createNormalized(JSON.parse(ent1Json));
			let lib = graph.getNodeById(parentId);
			let child = lib.addChildNode(parentProp);
			child.addToProp(ageProp, 20);
			child.addToProp(nameProp, 'helena')
			childId = child.getId();
			// console.log(lib);
			expect(lib[parentProp] instanceof ChildNode).to.be.true;
			expect(child.getType()).to.equal(ChildNode.TYPE)
			expect(child.getId()).to.match(/childnode#[a-z0-9-]{36}$/)
			await storage.persistDoc(lib, { create: true, skipChildNodes: false });
		});

		it('has saved the child', async() => {
			let qb = storage.createDescribeQuery();
			qb.setIncludeChildNodes(true);
			let res = await storage.load(qb);
			// console.dir(res, { depth: 9 })
			let child = res.getNodeById(childId);
			expect(child).to.not.be.undefined;
			expect(child instanceof ChildNode).to.be.true;
			expect(child.getLiteral(ageProp)).to.equal(20);
		});

		it('will load the child nodes with the parent', async() => {
			let qb = storage.createDescribeQuery();
			qb.addIdToFilter(parentId);
			let res = await storage.load(qb);
			// console.dir(res, { depth: 9 })
			let child = res.getNodeById(childId);
			expect(child).to.not.be.undefined;
			expect(child.getLiteral(ageProp)).to.equal(20);
			expect(child instanceof ChildNode).to.be.true;
			if (child instanceof ChildNode) {
				let parent = child.getParent();
				expect(parent.getType()).is.a('string')
			}
		});
	
	})
	
	

});
