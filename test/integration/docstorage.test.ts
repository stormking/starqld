import { describe, it, before, after } from 'mocha'
import { expect } from 'chai'
import { FusekiRepo, Graph, Node } from '../../src/index.js';
import DocumentStorage from '../../src/service/storage/document.js';


describe('DocumentStorage', async() => {
	
	let repo: FusekiRepo;
	let storage: DocumentStorage;
	before(async() => {
		repo = new FusekiRepo('http://admin:admin@localhost:3030/test1');
		await repo.destroyCollection();
		await repo.assertCollection();
		storage = await repo.assertDocumentStorage('doctest');
	});

	

	describe('basic function', () => {
		let id;
		let doc;
		const testData = {
			title: 'hello world',
			counter: 56,
			flag: true,
			nested: {
				something: {
					welp: 3
				},
				list: [
					{
						key: 'a',
						value: 1
					}, {
						key: 'b',
						value: 2
					}
				],
				emptyList: [],
				emptyObj: {}
			},
			emptyValue: null,
			dateValue: new Date()
		};
		it('can store simple data object', async() => {
			doc = await storage.storeData(testData);
			id = doc.getId();
			expect(id).to.be.a('string');
			expect(id.length).to.be.greaterThan(35);
			expect(doc.getRev()).to.equal(1);
			expect(doc.getCreated()).to.be.a('Date');
			expect(doc.getData()).to.deep.equal(testData);
		});

		it('can load the data by internal id', async() => {
			doc = await storage.load(id);
			let data = doc.getData();
			expect(data).to.deep.equal(testData);
		});

		it('can update the doc', async() => {
			testData.counter += 1;
			testData.nested.list.unshift({
				key: 'c',
				value: 499
			})
			testData.nested.list[2].value += 2
			doc.setData(testData);
			let updated = await storage.store(doc);
			expect(updated).to.be.true;
			expect(doc.getRev()).to.equal(2);
			expect(doc.getUpdated()).to.be.greaterThan(doc.getCreated());
		});

		it('can load the updated changes', async() => {
			doc = await storage.load(id);
			let data = doc.getData();
			expect(data).to.deep.equal(testData);
		});

		it('can delete the doc', async() => {
			await storage.delete(id);
		});

		it('can no longer load the doc', async() => {
			let error;
			try {
				let doc = await storage.load(id);
				console.error(doc.toJSON())
			} catch (e) {
				error = e;
			}
			expect(error).to.not.be.undefined;
			expect(error.message).to.include('found no doc')
			
		})

	})

});
