import { describe, it, before, after } from 'mocha'
import { expect } from 'chai'
import { FusekiRepo, Graph, Node } from '../../src/index.js';
import FS from 'fs';
import GraphStorage from '../../src/service/storage/graph.js';
import MultiGraphStorage from '../../src/service/storage/multigraph.js';
import DescribeBuilder from '../../src/service/qb/describe.js';

describe('Graph Storage', async() => {
	
	describe('one graph', () => {
		let repo: FusekiRepo
		let ent1Json: string
		let chapter : Node;
		let storage : GraphStorage;
		const chapterId = 'http://example.org/library/the-republic#introduction';
		const descriptionProp = 'http://purl.org/dc/elements/1.1/description';
		const newDescription = 'a better description';
		before(async() => {
			repo = new FusekiRepo('http://admin:admin@localhost:3030/test1');
			await repo.destroyCollection();
			await repo.assertCollection();
			storage = await repo.assertGraphStorage('test2');
			ent1Json = await FS.promises.readFile('./test/fixtures/library.jsonld', { encoding: 'utf8' })
		})
	
		it('can insert some data', async() => {
			let graph = await Graph.createNormalized(JSON.parse(ent1Json));
			await storage.persistGraph(graph, { create: true });
			// let res = await repo.upload(ent1Json);
			// console.dir(res, { depth: 9 })
			// expect(res.count).to.be.greaterThan(0);
		})
	
		it('can query via describe', async() => {
			let res = await storage.load(storage.createDescribeQuery());
			// console.dir(res, { depth: 9 })
			expect(res instanceof Graph).to.be.true;
			chapter = res.getNodeById(chapterId)!;
			expect(chapter instanceof Node).to.be.true;
		});
	
		it('can persist changes', async() => {
			chapter[descriptionProp] = newDescription;
			await storage.persistDoc(chapter);
		});
	
		it('can load persisted changes', async() => {
			let res = await storage.load(storage.createDescribeQuery());
			// console.dir(res, { depth: 9 })
			chapter = res.getNodeById(chapterId)!;
			expect(chapter.getLiteral(descriptionProp)).to.equal(newDescription);
		});
	})
	
	
	describe('named graphs', () => {
		let repo: FusekiRepo
		let ent1Json: string
		let storageOne: GraphStorage;
		let storageBoth: MultiGraphStorage;
		before(async() => {
			repo = new FusekiRepo('http://admin:admin@localhost:3030/test1');
			await repo.destroyCollection();
			await repo.assertCollection();
			ent1Json = await FS.promises.readFile('./test/fixtures/person1.jsonld', { encoding: 'utf8' })
			storageOne = await repo.assertGraphStorage('g1')
			storageBoth = await repo.assertMultiGraphStorage(['g1', 'g2'])
		})
		it('can insert some data', async() => {
			let res = await repo.upload(ent1Json);
			// console.dir(res, { depth: 9 })
			expect(res.count).to.be.greaterThan(0);
			expect(res.quadCount).to.be.greaterThan(0);
		})
		it('can find in named graph g1', async() => {
			let res = await storageOne.load(storageOne.createDescribeQuery());
			// console.dir(res, { depth: 9 })
			expect(res.getGraph().length).to.equal(1)
		});
		it('can find in named graph g1 and g2', async() => {
			let res = await storageBoth.load(storageBoth.createDescribeQuery());
			// console.dir(res, { depth: 9 })
			expect(res.getGraph().length).to.equal(2)
		});
		it('can update in specified graph g1', async() => {
			let res = await storageOne.load(storageOne.createDescribeQuery());
			let n: Node = res.getGraph().one;
			n.setLiteral('http://schema.org/jobHunt', true)
			let done = await storageOne.persistDoc(n)
		});
		it('can load changes in graph g1', async() => {
			let res = await storageOne.load(storageOne.createDescribeQuery());
			// console.dir(res, { depth: 9 })
			expect(res.getGraph().length).to.equal(1)
			let n: Node = res.getGraph().one;
			let change = n.getLiteral('http://schema.org/jobHunt')
			expect(change).to.be.true;
		});
	})

	describe('query over multiple graphs', () => {
		let repo: FusekiRepo
		let ent1Json: string
		let storageBoth: MultiGraphStorage;
		before(async() => {
			repo = new FusekiRepo('http://admin:admin@localhost:3030/test1');
			await repo.destroyCollection();
			await repo.assertCollection();
			ent1Json = await FS.promises.readFile('./test/fixtures/lib4.jsonld', { encoding: 'utf8' })
			storageBoth = await repo.assertMultiGraphStorage(['meta', 'data'])
		})
		it('can insert some data', async() => {
			let res = await repo.upload(ent1Json);
			// console.dir(res, { depth: 9 })
			expect(res.count).to.be.greaterThan(0);
			expect(res.quadCount).to.be.greaterThan(0);
		})
		it('can query', async() => {
			let qb = storageBoth.createDescribeQuery();
			qb.addCondition(DescribeBuilder.MAIN_SUB, 'a{2}', 'http://example.org/lendable')
			let res = await storageBoth.load(qb);
			// console.dir(res, { depth: 9 })
			expect(res.getGraph().length).to.equal(1)
		});
	})

});
