import { describe, it, before, after } from 'mocha'
import { expect } from 'chai'
import { FusekiRepo, Graph, Node } from '../../src/index.js';
import KeyValueStorage from '../../src/service/storage/keyvalue.js';

function sleep(ms) {
	return new Promise(res => {
		setTimeout(res, ms)
	})
}

describe('KeyValueStorage', async() => {
	
	let repo: FusekiRepo;
	let storage: KeyValueStorage;
	before(async() => {
		repo = new FusekiRepo('http://admin:admin@localhost:3030/test1');
		await repo.destroyCollection();
		await repo.assertCollection();
		storage = await repo.assertKeyValueStorage('kvtest');
	});

	async function getAndSet(key, value) {
		await storage.set(key, value);
		let loaded = await storage.get(key);
		expect(loaded).to.deep.equal(value);
	}

	describe('basic function', () => {
		it('can set and get a number', () => getAndSet('key1', 12345));
		it('can set and get a boolean', () => getAndSet('key2', true));
		it('can set and get a string', () => getAndSet('key3', 'hello'));
		it('can set and get an object', () => getAndSet('key4', { "hello": 123 }));
		it('can set and get an array', () => getAndSet('key5', [1, 4, 12]));

		it('can override a value', async() => {
			const key = 'key10';
			const value1 = 10;
			const value2 = 20;
			await storage.set(key, value1);
			await storage.set(key, value2);
			let loaded = await storage.get(key);
			expect(loaded).to.equal(value2);
		});

		it('can return default value for unknown keys', async() => {
			const defaultValue = 7;
			let loaded = await storage.get('unknownKey', defaultValue);
			expect(loaded).to.equal(defaultValue);
		});
	})
	

	describe('tags', () => {
		it('can set entries with tag', async() => {
			const key1 = 'k1';
			const key2 = 'k2';
			const tag1 = 't1';
			const tag2 = 't2';
			const value = 5;
			await storage.set(key1, value, 5, [tag1, tag2]);
			await storage.set(key2, value, 6, [tag1]);
			let list = await storage.listEntriesByTag(tag1);
			expect(list).to.deep.equal([[key1, value], [key2, value]])
		})
	})

	describe('expiration', () => {
		it('can retrieve before expiration', async() => {
			const key = 'key10';
			const value1 = 10;
			await storage.set(key, value1, 10);
			let loaded = await storage.get(key);
			expect(loaded).to.equal(value1);
		});
		it('will return defaultValue after expiration', async() => {
			const key = 'key10';
			const value1 = 10;
			const defaultValue = 5;
			await storage.set(key, value1, 1);
			await sleep(2000);
			let loaded = await storage.get(key, defaultValue);
			expect(loaded).to.equal(defaultValue);
		});
	})

});
