import { describe, it, before, after } from 'mocha'
import { expect } from 'chai'
import { FusekiRepo, Graph, Node } from '../../src/index.js';
import BaseRepo from '../../src/service/base-repo.js';
import BaseStorage from '../../src/service/storage/base.js';

function sleep(ms) {
	return new Promise(res => {
		setTimeout(res, ms)
	})
}

describe('MetaStorage', async() => {
	
	describe('first run', () => {
		let repo: FusekiRepo;
		before(async() => {
			repo = new FusekiRepo('http://admin:admin@localhost:3030/test1');
			await repo.destroyCollection();
			await repo.assertCollection();
		});

		
		it('can create a storage with just id', async() => {
			await repo.assertKeyValueStorage('s1')
			let metaGraph = await repo.getMetaRepo();
			let metaNode = metaGraph.getNodeById(BaseStorage.GRAPH_PREFIX+'s1');
			expect(metaNode[BaseRepo.GRAPH_TYPE_PROP]).to.deep.equal({ '@id': BaseRepo.GRAPH_TYPE_PREFIX+'kv' })
		})

		it('can create a storage with different graphName', async() => {
			await repo.assertKeyValueStorage({ id: 's2', graphName: 'http://starqld.cryptic.link/custom/s1new' })
		})

		it('cannot create a storage with a different type on the same id', async() => {
			let error;
			try {
				await repo.assertGraphStorage({ id: 's2' })
			} catch (e) {
				error = e;
			}
			expect(error).to.not.be.undefined;
			expect(error.message).to.include('want to init with type graph')
		})

		it('can add tags to a new graph', async() => {
			await repo.assertKeyValueStorage({
				id: 's3',
				tags: ['t1']
			})
		})

		it('can list all storages', async() => {
			let res = await repo.listStorages();
			expect(res.length).to.equal(3);
		})

		it('can list by tag name', async() => {
			let res = await repo.listStorages({ tags: ['t1'] });
			expect(res.length).to.equal(1);
		})

		it('can not override a storage with a different graphname', async() => {
			let error;
			try {
				await repo.assertKeyValueStorage({ id: 's1', graphName: 's1new' })
			} catch(e) {
				error = e;
			}
			expect(error).to.not.be.undefined;
			expect(error.message).to.include('expected graph to be name s1new')
		})

		it('can delete a graph', async() => {
			await repo.deleteStorage('s3');
		})

		it('can do multiple operations at once')

		after(async () => {
			await sleep(400);
			//wait for saving
		})
	})

	describe('second run', () => {
		let repo: FusekiRepo;
		before(async() => {
			repo = new FusekiRepo('http://admin:admin@localhost:3030/test1');
		});

		it('cannot create a storage with a different type on the same id', async() => {
			let error;
			try {
				await repo.assertGraphStorage({ id: 's2' })
			} catch (e) {
				error = e;
			}
			expect(error).to.not.be.undefined;
			expect(error.message).to.include('want to init with type graph')
		})

		
		it('can load an existing storage with just id', async() => {
			await repo.assertKeyValueStorage('s1')
			let metaGraph = await repo.getMetaRepo();
			let metaNode = metaGraph.getNodeById(BaseStorage.GRAPH_PREFIX+'s1');
			expect(metaNode[BaseRepo.GRAPH_TYPE_PROP]).to.deep.equal({ '@id': BaseRepo.GRAPH_TYPE_PREFIX+'kv' })
		})
	})
	

});
