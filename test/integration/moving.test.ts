import { describe, it, before, after } from 'mocha'
import { expect } from 'chai'
import { FusekiRepo, LinkedDoc, Node } from '../../src/index.js';
import FS from 'fs';

describe('graph operations', async() => {
	let repo: FusekiRepo
	let ent1Json: string
	let ent2Json: string
	let chapter : Node;
	let chId = 'http://example.org/library/the-republic';
	let libId = 'http://example.org/library';
	let contains = 'http://example.org/vocab#contains';

	before(async() => {
		ent1Json = await FS.promises.readFile('./test/fixtures/library2.jsonld', { encoding: 'utf8' })
		ent2Json = await FS.promises.readFile('./test/fixtures/library3.jsonld', { encoding: 'utf8' })
	})

	it('can move to empty graph incl subnodes', async() => {
		let from = await LinkedDoc.createNormalized(JSON.parse(ent1Json));
		let to = LinkedDoc.createEmpty();
		let oldNode = from.getNodeById(chId);
		await to.addExternalNode(oldNode, { depth: 2 });
		// to.getGraph().one['a'] = 3;
		// console.dir(to, { depth: 9 })
		let newNode = to.getNodeById(chId);
		expect(newNode).to.not.equal(oldNode);
		expect(to.getGraph().length).to.equal(3);
	})

	it('can move to empty graph incl 1 sub', async() => {
		let from = await LinkedDoc.createNormalized(JSON.parse(ent1Json));
		let to = LinkedDoc.createEmpty();
		let oldNode = from.getNodeById(chId);
		await to.addExternalNode(oldNode, { depth: 1 });
		// console.dir(to, { depth: 9 })
		let newNode = to.getNodeById(chId);
		expect(newNode).to.not.equal(oldNode);
		expect(to.getGraph().length).to.equal(3);
	})

	it('can move to empty graph excl subnodes', async() => {
		let from = await LinkedDoc.createNormalized(JSON.parse(ent1Json));
		let to = LinkedDoc.createEmpty();
		let oldNode = from.getNodeById(chId);
		await to.addExternalNode(oldNode, { depth: 0 });
		// console.dir(to, { depth: 9 })
		expect(to.getGraph().length).to.equal(2);
	})

	it('can move to graph with existing lib object and merge that', async() => {
		let from = await LinkedDoc.createNormalized(JSON.parse(ent1Json));
		let to = await LinkedDoc.createNormalized(JSON.parse(ent2Json));
		let oldNode = from.getNodeById(chId);
		await to.addExternalNode(oldNode, { depth: 2 });
		// console.dir(to, { depth: 3 })
		let newNode = to.getNodeById(chId);
		expect(newNode).to.not.equal(oldNode);
		expect(to.getGraph().length).to.equal(5);
		let lib = to.getNodeById(libId);
		let coll = lib.getAll(contains);
		expect(coll.length).to.equal(2);
	})

	it('can fully merge 2 graphs', async() => {
		let from = await LinkedDoc.createNormalized(JSON.parse(ent1Json));
		let to = await LinkedDoc.createNormalized(JSON.parse(ent2Json));
		to.merge(from);
		// console.dir(to, { depth: 9 })
		// console.log(to.getGraph().map(e => e.getId()))
		expect(to.getGraph().length).to.equal(6);
	})
	

});
