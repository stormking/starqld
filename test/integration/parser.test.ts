import { describe, it, before, after } from 'mocha'
import { FusekiRepo, LinkedDoc, Node } from '../../src/index.js';
import FS from 'fs';
import { expect } from 'chai';

describe('parser', () => {

	const names = ['activity', 'event', 'library', 'person', 'place', 'product', 'recipe']
	let rdfs: any[] = [];
	for (let name of names) {
		let file = FS.readFileSync('./test/fixtures/'+name+'.jsonld', { encoding: 'utf8' });
		rdfs.push( JSON.parse(file) )
	}
	// console.log(rdfs)
	before(async() => {
		
	})

	function initGraph(rdf) {
		return async() => {
			let res = await LinkedDoc.createNormalized(rdf)
			// console.dir(res, { depth: 99 });
			expect(res instanceof LinkedDoc).to.be.true;
			expect(res.getGraph().length).to.be.greaterThan(0);
		}
	}

	names.forEach((name, i) => {
		// console.log('init', i, name, rdfs[i])
		it('can init '+name, initGraph(rdfs[i]));
	})

	

});
