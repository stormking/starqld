import { describe, it, before, after } from 'mocha'
import { expect } from 'chai'
import { FusekiRepo, Graph, Node } from '../../src/index.js';
import FS from 'fs';
import GraphStorage from '../../src/service/storage/graph.js';
import ChildNode from '../../src/model/child-node.js';

class Library extends Node {

}

describe('Subclassing', async() => {
	
	describe('Basics', () => {
		let ent1Json: string
		const bookId = 'http://example.org/library/the-republic';
		const libId = 'http://example.org/library';
		const libType = 'http://example.org/vocab#Library';
		let graph: Graph
		before(async() => {
			ent1Json = await FS.promises.readFile('./test/fixtures/library.jsonld', { encoding: 'utf8' })
		})
	
		it('can create a graph', async() => {
			graph = await Graph.createNormalized(JSON.parse(ent1Json), function(data, defaultConstr) {
				// console.log('cb', data);
				if (data['@type'] === libType) {
					return Library
				}
				return defaultConstr;
			});
			// console.log(graph.getGraph())
			let libNode = graph.getNodeById(libId)
			expect(libNode).instanceOf(Library)
			let bookNode = graph.getNodeById(bookId);
			expect(bookNode).not.instanceOf(Library)
			expect(bookNode).instanceOf(Node)
		});

		it('can add a subclassed node', async() => {
			let lib2 = graph.addNewNodeWithId(libId+'2', libType);
			expect(lib2).instanceOf(Library)
		});

		it('can correctly merge graphs', async() => {
			let graph2 = await Graph.createNormalized(JSON.parse(ent1Json));
			let graph3 = await Graph.createNormalized([], function(data, defaultConstr) {
				// console.log('cb', data);
				if (data['@type'] === libType) {
					return Library
				}
				return defaultConstr;
			});
			graph3.merge(graph2);
			let libNode = graph3.getNodeById(libId)
			expect(libNode).instanceOf(Library)

		});
	
	})
	
	

});
