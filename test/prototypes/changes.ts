import { describe, it, before, after } from 'mocha'
import { expect } from 'chai'
import jsonld from 'jsonld'
import { diffLines } from 'diff';
import FS from 'fs';
import '../../src/document-loader';

describe('calculate changes', async() => {

	let rdf;
	before(async() => {
		rdf = JSON.parse(await FS.promises.readFile('./test/fixtures/person.jsonld', { encoding: 'utf8' }))
	})

	// const oldLoader =jsonld.documentLoader;
	// const ld: any = jsonld;
	// const oldLoader = ld._documentLoader;

	const rdfOpts: jsonld.Options.ToRdf = {
		format: 'application/n-quads'
	}

	async function getChanges(oldData, newData) {
		const oldRdf: any = await jsonld.toRDF(oldData, rdfOpts);
		const newRdf: any = await jsonld.toRDF(newData, rdfOpts);
		// const oldRdf: any = await jsonld.normalize(oldData);
		// const newRdf: any = await jsonld.normalize(newData);
		// console.log(oldRdf, newRdf)
		let res = diffLines(oldRdf, newRdf)
		// console.dir(res, { depth: 9 })
		const added: string[] = [];
		const removed: string[] = [];
		res.forEach(e => {
			if (e.removed) removed.push(e.value.trim())
			else if (e.added) added.push(e.value.trim())
		});
		return { added, removed }
	}

	it('can insert some data', async() => {
		let newRdf = JSON.parse(JSON.stringify(rdf));
		newRdf.name = 'James Cameron';
		await getChanges(rdf, newRdf)
	})

	

});
