import { FusekiRepo, Graph, Literal } from '../../src/index';
//configure the endpoint
const repo = new FusekiRepo('http://admin:admin@localhost:3030/test1')
//create test1 if it does not exist
await repo.assertCollection();
const id1 = 'http://example.org/test/entity1';
const prop1 = 'http://example.org/test/prop/label';
// see https://json-ld.org/ for the data syntax
let graph = await Graph.createNormalized({
    '@id': id1,
    '@type': 'http://example.org/test/type1',
    [prop1]: 'hello world'
});
//handling the nodes
let node = graph.getNodeById(id1);
node[prop1] = 'hello change'
console.log(node.get(prop1))    // same as node[prop1] - Literal{ @value: 'hello change' }
console.log(node.getLiteral(prop1)) //hello change (parses value according to type)
console.log(node.getAll(prop1)) // Collection[ Literal{ @value: 'hello change' } ] (always returns as collection)

node.addToProp(prop1, new Literal({ '@value': 'hole mundo', '@language': 'es' }))
//this adds to the prop instead of overwriting it
console.log(node.get(prop1))    //Collection[ Literal{ @value: 'hello change' }, Literal{ @value: 'hola mundo', @language: 'es' } ]

//init a named graph for storage
const storage = repo.assertGraphStorage('store1')
await storage.persistGraph(graph, { create: true }) 
// the create option will persist all data, not just that which was changed after init


//load the data
const qb = storage.createDescribeQuery();
qb.addIdToFilter(id1);
graph = await storage.load(qb);
