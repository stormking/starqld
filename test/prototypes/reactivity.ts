import { describe, it, before, after } from 'mocha'
import vuex from 'vuex';
import LinkedDoc from '../../src/linked-doc';
import FS from 'fs';

describe('reactivity', async() => {

	let rdf, store;
	before(async() => {
		rdf = JSON.parse(await FS.promises.readFile('./test/fixtures/recipe.jsonld', { encoding: 'utf8' }))
	})

	it('can run a store', async() => {
		store = new vuex.Store({
			state: {
				root: null
			},
			mutations: {
				init(state, r) {
					state.root = r;
				}
			}
		});
		console.log(store);
		store.commit('init', { a: 5 });
		console.log(store.state);
	})

	it('can add doc', async() => {
		let doc = await LinkedDoc.createNormalized(rdf);
		store.commit('init', doc);
		doc.getGraph().find(e => e['@id'] === '_:b1').bla = 'blub';
		console.dir(store.state, { depth: 99 });
	})

	

});
