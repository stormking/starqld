import { describe, it, before, after } from 'mocha'
import { expect } from 'chai'
import FusekiRepo from '../../src/fuseki';
import Validator from '../../src/validator';
import FS from 'fs';
import '../../src/document-loader';

describe('Validator', async() => {

	let rdf, repo;
	before(async() => {
		rdf = JSON.parse(await FS.promises.readFile(__dirname+'/../fixtures/person.jsonld', { encoding: 'utf8' }))
	})

	before(async() => {
		repo = new FusekiRepo('http://admin:admin@localhost:3030/test1');
		await repo.destroyCollection();
		await repo.assertCollection();
		let ent1Json = await FS.promises.readFile(__dirname+'/../fixtures/library.jsonld', { encoding: 'utf8' })
		let res = await repo.upload(ent1Json);
	})

	it('can run full validation', async() => {
		const v = new Validator(repo);
		await v.run();
		let errors = v.getErrors();
		console.log(errors);
	})

	

});
